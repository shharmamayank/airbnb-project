import React from 'react'
import "./Section3.css"

export default function Section3({ img,
    rating,
    reviewCount,
    location,
    title,
    price }) {
    return (
        <>

            <div className='section3-card-1'>
                <img className='card-image' src={img} alt="" />
                <div>
                    <img src="./Star.png" alt="" />
                    <span>{rating}
                    </span>
                    <span>r{reviewCount}</span>
                    <span>{location}</span>
                </div>
                <p>{title}</p>
                <p><span><b>From {price} </b></span>/ person</p>
            </div>


        </>
    )
}
