import React from 'react'

export default function Section2() {
  return (
    <>
      <div className='section2-container'>
        <h1>Online Experiences</h1>
        <p>Join unique interactive activities led by one-of-a-kind hosts—all without leaving home.
        </p>
      </div>
    </>
  )
}
