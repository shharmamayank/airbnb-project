import './App.css';
import NavBar from "./component/NavBar";
import HeroSection from './component/HeroSection';
import Section2 from './component/Section2';
import Section3 from './component/Section3';
import data from "./data";
function App() {
  const cards = data.map(data => {

    return (
      <>
        <div className='contact'>
          <Section3
            img={data.img}
            rating={data.stats.rating}
            reviewCount={data.stats.reviewCount}
            location={data.location}
            title={data.title}
            price={data.price} />
        </div>
      </>
    )
  })

  return (
    <>
      <NavBar />
      <HeroSection />
      <Section2 />
      <div className='section3-card'>{cards}</div>
    </>
  );

}

export default App;
